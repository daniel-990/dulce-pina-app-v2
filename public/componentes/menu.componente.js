import {moduleMenu} from '../json/menu.js';

let dataMenu = moduleMenu.mostrarInformacion();
   
   export const menu = () =>{
        
        const menuLiheader = document.querySelector("#menu-li");
        const menu = document.querySelector("#menu-app");
        
        let htmlMenu;
        let htmlMenuLi;

        htmlMenuLi = `
            <div class="user-view">
                <div class="background">
                    <img src="${dataMenu.imagen}">
                </div>
                <a href="#user"><img class="circle" src="${dataMenu.logo}"></a>
                <a href="#name"><span class="white-text name">${dataMenu.titulo}</span></a>
                <a href="mailto:${dataMenu.correo}"><span class="white-text email">${dataMenu.correo}</span></a>
            </div>
        `;
        menuLiheader.innerHTML += htmlMenuLi;

        dataMenu.menu.map(menuPrincipal => {
            htmlMenu = `
                <li><a href="#!"><i class="material-icons">${menuPrincipal.icono}</i>${menuPrincipal.nombre}</a></li>
            `;
            menu.innerHTML += htmlMenu;
        })

        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
          });
    }