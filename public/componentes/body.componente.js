import {moduleFirebaseData} from '../json/fisebase-connet.js';

let firebaseData = moduleFirebaseData.mostrarInformacion();

firebase.initializeApp(firebaseData.config); //se inicia firebase
const prendas = firebase.database().ref('prendas/');

const htmlAppp = document.querySelector("#app");
let htmlCatalogo;

    export const body = () => {

        prendas.on('child_added', function(data) {
          console.log(data.val()[2]);
          htmlCatalogo = `
            <li>
                <div class="collapsible-header">
                    <i class="material-icons">loyalty</i>
                    ${data.val()[2]}
                    <span class="badge color-numero z-depth-2">${data.val()[3]} unidades</span></div>
                <div class="collapsible-body">
                    <p>
                        <b>Precio docena:</b> <span class="badge color-numero-p">${data.val()[5]}$</span>
                    </p>
                    <p>
                        <b>Precio media docena:</b> <span class="badge color-numero-p">${data.val()[4]}$</span>
                    </p>
                </div>
            </li>
          `;
          htmlAppp.innerHTML += htmlCatalogo;
        });

        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.collapsible');
            var instances = M.Collapsible.init(elems);
        });
    }